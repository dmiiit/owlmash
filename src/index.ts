function unreachable(_: never): never {
    throw new Error("unreachable");
}

const LUL = `<img src="https://static-cdn.jtvnw.net/emoticons/v1/425618/2.0" alt="LUL" title="LUL" />`;

enum Role {
    Offense = "offense",
    Tank    = "tank",
    Support = "support",
}

interface Team {
    id: number;
    name: string;
    abbreviatedName: string;
    primaryColor: string;
    secondaryColor: string;
    icon: string;
}

interface TeamEntry {
    team: Team;
}

interface PlayerAttributes {
    role: Role;
    heroes?: string[];
}

interface Player {
    id: number;
    name: string;
    givenName: string;
    familyName: string;
    teams: TeamEntry[];
    attributes: PlayerAttributes;
}

class TeamName {
    location: string;
    nickname: string;

    constructor(name1: string, name2: string) {
        const [location1, nickname1] = TeamName.splitName(name1);
        const [location2, nickname2] = TeamName.splitName(name2);

        this.location = TeamName.mergeLocations(location1, location2);
        this.nickname = TeamName.mergeNicknames(nickname1, nickname2);
    }

    full() {
        return this.location + " " + this.nickname;
    }

    private static splitName(fullName: string): [string, string] {
        const lastSpace = fullName.lastIndexOf(" ");

        // Some teams have a location consisting of multiple words, e.g. Los Angeles
        const location = fullName.slice(0, lastSpace);

        // Team nickname is always the last word of the full team name
        const nickname = fullName.slice(lastSpace + 1);

        return [location, nickname];
    }

    private static splitLocation(loc: string): [string, string] {
        switch (loc) {
            case "Atlanta":       return ["Atla", "lanta"];
            case "Boston":        return ["Bos", "ton"];
            case "Chengdu":       return ["Cheng", "du"];
            case "Dallas":        return ["Dal", "las"];
            case "Florida":       return ["Flo", "rida"];
            case "Guangzhou":     return ["Guang", "zhou"];
            case "Hangzhou":      return ["Hang", "zhou"];
            case "Houston":       return ["Hou", "ston"];
            case "London":        return ["Lon", "don"];
            case "Los Angeles":   return ["Los ", "Angeles"];
            case "New York":      return ["New ", "York"];
            case "Paris":         return ["Pa", "ris"];
            case "Philadelphia":  return ["Phila", "delphia"];
            case "San Francisco": return ["San ", "Francisco"];
            case "Seoul":         return ["Seo", "ul"];
            case "Shanghai":      return ["Shang", "hai"];
            case "Toronto":       return ["To", "ronto"];
            case "Vancouver":     return ["Van", "couver"];
            case "Washington":    return ["Washing", "ton"];
            default:
                const spaceAt = loc.indexOf(" ");
                if (spaceAt > 0) {
                    return [loc.slice(0, spaceAt + 1), loc.slice(spaceAt + 1)];
                } else {
                    const middle = loc.length / 2;
                    return [loc.slice(0, middle), loc.slice(middle)];
                }
        }
    }

    private static mergeLocations(loc1: string, loc2: string, algo: "short" | "long" | "custom" = "custom"): string {
        if (loc1 == loc2) {
            return loc1;
        }

        const isUpper: (ch: string) => boolean = (ch) => ch.toUpperCase() === ch;

        if (algo == "custom") {
            const left = TeamName.splitLocation(loc1)[0];
            const right = TeamName.splitLocation(loc2)[1];

            if (left.endsWith(" ")) {
                if (isUpper(right[0])) {
                    return left + right;
                }
                return left + loc2;
            }

            if (isUpper(right[0])) {
                return left + right.toLowerCase();
            }

            return left + right;
        }

        let left: string;
        let right: string;

        const spaceAt1 = loc1.indexOf(" ");
        if (spaceAt1 > 0) {
            left = loc1.slice(0, spaceAt1 + 1);
        } else {
            if (algo === "short") {
                left = loc1.slice(0, loc1.length / 2);
            } else {
                left = loc1.slice(0, Math.ceil(loc1.length / 2.0));
            }
        }

        const spaceAt2 = loc2.indexOf(" ");
        if (spaceAt2 > 0) {
            right = loc2.slice(spaceAt2 + 1);
        } else if (spaceAt1 > 0) {
            right = loc2;
        } else {
            right = loc2.slice(loc2.length / 2);
        }

        return (left + right);
    }

    private static splitNickname(nick: string): [string, string] {
        switch (nick) {
            case "Charge":     return ["Char", "arge"];
            case "Defiant":    return ["Def", "fiant"];
            case "Dragons":    return ["Dra", "gons"];
            case "Dynasty":    return ["Dyn", "sty"];
            case "Eternal":    return ["Eter", "nal"];
            case "Excelsior":  return ["Excel", "elsior"];
            case "Fuel":       return ["Fue", "uel"];
            case "Fusion":     return ["Fus", "sion"];
            case "Gladiators": return ["Glad", "ators"];
            case "Hunters":    return ["Hun", "ters"];
            case "Justice":    return ["Just", "stice"];
            case "Mayhem":     return ["May", "hem"];
            case "Outlaws":    return ["Out", "laws"];
            case "Reign":      return ["Rei", "eign"];
            case "Shock":      return ["Sho", "ock"];
            case "Spark":      return ["Spa", "ark"];
            case "Spitfire":   return ["Spit", "fire"];
            case "Titans":     return ["Tit", "tans"];
            case "Uprising":   return ["Up", "rising"];
            case "Valiant":    return ["Val", "liant"];
            default:
                const middle = nick.length / 2;
                return [nick.slice(0, middle), nick.slice(middle)];
        }
    }

    private static mergeNicknames(name1: string, name2: string, algo: "short" | "long" | "custom" = "custom"): string {
        if (name1 == name2) {
            return name1;
        }

        switch (algo) {
            case "short":
                return name1.slice(0, name1.length / 2) + name2.slice(name2.length / 2);
            case "long":
                return name1.slice(0, Math.ceil(name1.length / 2.0)) + name2.slice(name2.length / 2);
            case "custom":
                const left = TeamName.splitNickname(name1)[0];
                const right = TeamName.splitNickname(name2)[1];
                return left + right;
        }
    }
}

class Color {
    r: number;
    g: number;
    b: number;
    a: number;

    constructor(r: number, g: number, b: number, a?: number) {
        this.r = r;
        this.g = g;
        this.b = b;
        this.a = a || 1.0;
    }

    static fromHex(hex: string): Color {
        const r = parseInt(hex.slice(0, 2), 16);
        const g = parseInt(hex.slice(2, 4), 16);
        const b = parseInt(hex.slice(4, 6), 16);
        return new Color(r, g, b);
    }

    asCss(alpha?: number): string {
        return `rgba(${this.r}, ${this.g}, ${this.b}, ${alpha || this.a || 1.0})`;
    }

    luminance(): number {
        return (0.299 * this.r + 0.587 * this.g + 0.114 * this.b) / 255.0;
    }
}

class TeamColors {
    primary: Color;
    secondary: Color;

    constructor(primary: Color, secondary: Color) {
        this.primary = primary;
        this.secondary = secondary;
    }
}

class MergedTeam {
    name: TeamName;
    colors1: TeamColors;
    colors2: TeamColors;
    //TODO: icon

    constructor(t1: Team, t2: Team) {
        this.name = new TeamName(t1.name, t2.name);
        this.colors1 = new TeamColors(Color.fromHex(t1.primaryColor), Color.fromHex(t1.secondaryColor));
        this.colors2 = new TeamColors(Color.fromHex(t2.primaryColor), Color.fromHex(t2.secondaryColor));
    }
}

class HeroPool {
    main: string[];
    flex: string[];

    constructor(main: string[], flex: string[]) {
        this.main = main.map(HeroPool.prettyHeroName);
        this.flex = flex.map(HeroPool.prettyHeroName);
    }

    static combined(pool1: string[] = [], pool2: string[] = []): HeroPool {
        // Signature heroes are the ones shared by both
        const main = pool1
            .filter(h => pool2.indexOf(h) >= 0)
            .sort();

        // Flex heroes are the ones played by only one of the two players
        const unique1 = pool1.filter(h => pool2.indexOf(h) < 0);
        const unique2 = pool2.filter(h => pool1.indexOf(h) < 0);
        const flex = [...unique1, ...unique2]
            .sort();

        return new HeroPool(main, flex);
    }

    static oneTrick(hero: string) {
        return new HeroPool([hero], []);
    }

    private static prettyHeroName(s: string): string {
        switch (s) {
            case "dva":           return "D.Va";
            case "mccree":        return "McCree";
            case "soldier-76":    return "Soldier: 76";
            case "wrecking-ball": return "Wrecking Ball";
            default:              return s.replace(/(?:^|\s)\S/g, a => a.toUpperCase());
        }
    }
}

class MergedPlayer {
    handle: string;
    givenName: string;
    familyName: string;
    team: MergedTeam;
    role: string;
    heroes: HeroPool;

    constructor(p1: Player, p2: Player) {
        this.handle = MergedPlayer.mergeHandles(p1.name, p2.name);
        this.givenName = p1.givenName;
        this.familyName = p2.familyName;

        const team1 = p1.teams[0].team;
        const team2 = p2.teams[0].team;
        this.team = new MergedTeam(team1, team2);

        this.role = MergedPlayer.playerRole(p1, p2);
        this.heroes = MergedPlayer.heroPool(p1, p2);
    }

    fullName(): string {
        return this.givenName + " " + this.familyName;
    }

    private static mergeHandles(h1: string, h2: string, algo: "short" | "long" = "short"): string {
        // This one is grandfathered in
        if (h1 === "BIRDRING" && h2 === "Rascal") { return "BIRDscal"; }

        if (h1 === h2) {
            switch (h1) {
                case "Jake":
                    return `J ${LUL} K E`;
                case "ArHaN":
                    return `ARHAN ${LUL}`;
                case "EscA":
                    return `ESCA ${LUL}`;
                default:
                    return `${h1}²`;
            }
        }

        //TODO: use a better algo to split names?
        switch (algo) {
            case "short":
                return h1.slice(0, Math.ceil(h1.length / 2.0)) + h2.slice(h2.length / 2);
            case "long":
                return h1.slice(0, h1.length / 2) + h2.slice(h2.length / 2);
        }
    }

    private static playerRole(p1: Player, p2: Player): string {
        // Special cases for some of the players
        if (p1.name == p2.name) {
            switch (p1.name) {
                case "EscA":
                    return "Captain";
                case "Geguri":
                    return "Frog";
                case "Jake":
                    return "Jakerat";
                case "JJONAK":
                    return "Omae wa mou shindeiru";
            }
        }

        return MergedPlayer.combinedRole(p1.attributes.role, p2.attributes.role);
    }

    //TODO: better role inference by examining hero pool? (main/off tank, main/flex support, etc)
    private static combinedRole(r1: Role, r2: Role): string {
        switch (r1) {
            case Role.Offense:
                switch (r2) {
                    case Role.Offense:
                        return "DPS God";
                    case Role.Tank:
                        return "Aggro Tank";
                    case Role.Support:
                        return "Bloodthirsty Support";
                    default:
                        return unreachable(r2);
                    }
    
            case Role.Tank:
                switch (r2) {
                    case Role.Offense:
                        return "Fat Genji";
                    case Role.Tank:
                        return "Tank Main";
                    case Role.Support:
                        return "&quot;I'll fill&quot;";
                    default:
                        return unreachable(r2);
                }
    
            case Role.Support:
                switch (r2) {
                    case Role.Offense:
                        return "Battle Angel";
                    case Role.Tank:
                        return "&quot;Wanna go GOATS?&quot;";
                    case Role.Support:
                        return "Career Healer";
                    default:
                        return unreachable(r2);
                }
    
            default:
                return unreachable(r1);
        }
    }

    private static heroPool(p1: Player, p2: Player): HeroPool {
        // Special cases for some players
        if (p1.name == p2.name) {
            switch (p1.name) {
                case "aKm":
                    return HeroPool.oneTrick("genji");
                case "ameng":
                    return new HeroPool(["wrecking-ball"], ["orisa", "reinhardt", "winston"]);
                case "Jake":
                    return HeroPool.oneTrick("junkrat");
            }
        }

        return HeroPool.combined(p1.attributes.heroes, p2.attributes.heroes);
    }
}

const GRANDFATHERED_PLAYERS: Player[] = [
    {
        id: 0,
        name: "EscA",
        givenName: "In-Jae",
        familyName: "Kim",
        teams: [
            {
                team: {
                    id: 0,
                    name: "Lunatic Hai",
                    abbreviatedName: "LH",
                    primaryColor: "324089",
                    secondaryColor: "f5f5f5",
                    icon: "string",
                }
            }
        ],
        attributes: {
            role: Role.Offense,
            heroes: ["mei", "sombra", "tracer", "mccree"],
        }
    }
];

function loadPlayers(cb: (players: Player[]) => void) {
    fetch("https://api.overwatchleague.com/players?locale=en_US")
        .then(resp => resp.json())
        .then(json => {
            const players = [...<Player[]>json.content, ...GRANDFATHERED_PLAYERS]
                .sort((a, b) => {
                    const aName = a.name.toLowerCase();
                    const bName = b.name.toLowerCase();
                    if      (aName < bName) { return -1 }
                    else if (aName > bName) { return  1 }
                    else                    { return  0 }
                });
            cb(players);
        });
}

let playersById: Map<number, Player> = new Map();

function mergePlayers() {
    const select1 = <HTMLSelectElement>document.getElementById("player1");
    const select2 = <HTMLSelectElement>document.getElementById("player2");

    const player1Id = parseInt(select1.selectedOptions[0].value, 10);
    const player2Id = parseInt(select2.selectedOptions[0].value, 10);

    const player1 = playersById.get(player1Id)!;
    const player2 = playersById.get(player2Id)!;

    const merged = new MergedPlayer(player1, player2);

    const result       = <HTMLDivElement>document.getElementById("result");
    const handle       = <HTMLHeadingElement>document.getElementById("resHandle");
    const name         = <HTMLSpanElement>document.getElementById("resName");
    const teamLocation = <HTMLSpanElement>document.getElementById("resTeamLocation");
    const teamNickname = <HTMLSpanElement>document.getElementById("resTeamNickname");
    const role         = <HTMLSpanElement>document.getElementById("resRole");
    const heroes       = <HTMLTableDataCellElement>document.getElementById("resHeroes");

    result.style.borderLeftColor   = merged.team.colors1.primary.asCss();
    result.style.borderTopColor    = merged.team.colors1.primary.asCss();
    result.style.borderRightColor  = merged.team.colors2.secondary.asCss();
    result.style.borderBottomColor = merged.team.colors2.secondary.asCss();

    handle.innerHTML = merged.handle;
    name.innerHTML = merged.fullName();
    role.innerHTML = merged.role;
    heroes.innerHTML = "";
    merged.heroes.main.forEach((hero, idx) => {
        const el = document.createElement("span");
        el.classList.add("heroMain");
        el.innerHTML = hero;
        heroes.appendChild(el);

        if (idx < merged.heroes.main.length - 1 || merged.heroes.flex.length > 0) {
            const delim = document.createTextNode(", ");
            heroes.appendChild(delim);
        }
    });
    merged.heroes.flex.forEach((hero, idx) => {
        const el = document.createElement("span");
        el.innerHTML = hero;
        heroes.appendChild(el);

        if (idx < merged.heroes.flex.length - 1) {
            const delim = document.createTextNode(", ");
            heroes.appendChild(delim);
        }
    });

    teamLocation.innerHTML = merged.team.name.location;
    teamLocation.style.color = merged.team.colors1.primary.asCss();
    if (merged.team.colors1.primary.luminance() > 0.5) {
        teamLocation.style.textShadow = "-1px -1px 0 #333, 1px -1px 0 #333, -1px 1px 0 #333, 1px 1px 0 #333";
    } else {
        teamLocation.style.textShadow = null;
    }

    teamNickname.innerHTML = merged.team.name.nickname;
    teamNickname.style.color = merged.team.colors2.secondary.asCss();
    if (merged.team.colors2.secondary.luminance() > 0.5) {
        teamNickname.style.textShadow = "-1px -1px 0 #333, 1px -1px 0 #333, -1px 1px 0 #333, 1px 1px 0 #333";
    } else {
        teamNickname.style.textShadow = null;
    }
}

function selectRandomPlayers(first: boolean, second: boolean) {
    if (first) {
        const player = <HTMLSelectElement>document.getElementById("player1");
        const rand = Math.random() * player.options.length;
        player.selectedIndex = rand;
    }

    if (second) {
        const player = <HTMLSelectElement>document.getElementById("player2");
        const rand = Math.random() * player.options.length;
        player.selectedIndex = rand;
    }

    mergePlayers();
}

function swapPlayers() {
    const player1 = <HTMLSelectElement>document.getElementById("player1");
    const player2 = <HTMLSelectElement>document.getElementById("player2");

    const tmp = player2.selectedIndex;
    player2.selectedIndex = player1.selectedIndex;
    player1.selectedIndex = tmp;

    mergePlayers();
}

function init() {
    const player1 = <HTMLSelectElement>document.getElementById("player1");
    const player2 = <HTMLSelectElement>document.getElementById("player2");
    const randomizeButton       = <HTMLButtonElement>document.getElementById("btnRandomize");
    const randomizeFirstButton  = <HTMLButtonElement>document.getElementById("btnRandomizeFirst");
    const randomizeSecondButton = <HTMLButtonElement>document.getElementById("btnRandomizeSecond");
    const swapButton            = <HTMLButtonElement>document.getElementById("btnSwap");

    player1.onchange = mergePlayers;
    player2.onchange = mergePlayers;
    randomizeButton.onclick       = () => selectRandomPlayers(true, true);
    randomizeFirstButton.onclick  = () => selectRandomPlayers(true, false);
    randomizeSecondButton.onclick = () => selectRandomPlayers(false, true);
    swapButton.onclick            = () => swapPlayers();

    loadPlayers(players => {
        players.forEach(player => {
            playersById.set(player.id, player);

            const opt = document.createElement("option");
            opt.value = player.id.toString();
            opt.innerText = player.name;

            player1.appendChild(opt.cloneNode(true));
            player2.appendChild(opt);
        });

        selectRandomPlayers(true, true);
    });
}