# Overwath League Player Fusion

Create mashups of Overwatch League players. Inspired by the [Pokemon fusion](http://pokemon.alexonsager.net) project.

![Screenshot](image.png)

## Build
Requirements: [TypeScript compiler](https://www.typescriptlang.org).

Run
```
tsc
```
to build the JavaScript code from TypeScript sources. Open `index.html` in a web browser to see the result.

## License
This software is released into the [public domain](LICENSE).
